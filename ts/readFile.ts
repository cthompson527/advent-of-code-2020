import { from, fromEvent } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import readline from 'readline';
import fs from 'fs';

function getFileObservable<T>(filePath: string) {
  const rl = readline.createInterface({ input: fs.createReadStream(filePath) });
  return fromEvent<T>(rl, 'line')
    .pipe(
      takeUntil(fromEvent(rl, 'close')),
    );
}

export default getFileObservable;
