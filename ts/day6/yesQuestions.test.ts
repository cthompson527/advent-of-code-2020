import { from } from "rxjs";
import { communalAnswers, countGroupAllAnswers, countGroupAnswers, removeDuplicates } from "./yesQuestions";

describe('day six', () => {
  test('removeDuplicates properly removes the duplicates in the string', () => {
    expect(removeDuplicates('aaaabb')).toBe('ab');
    expect(removeDuplicates('abcdeef')).toBe('abcdef');
  });

  test('countGroupAnswers returns the correct number of yes in the groups', async () => {
    const answers = [
      'abc',
      '',
      'a',
      'b',
      'c',
      '',
      'ab',
      'ac',
      '',
      'a',
      'a',
      'a',
      'a',
      '',
      'b',
    ];
    expect(await countGroupAnswers(from(answers))).toBe(11);
  });

  test('communalAnswers returns only the answers that are answered by all of the people', () => {
    const testInput = 'abd bd ab';
    expect(communalAnswers(testInput)).toBe('b');
  });

  test('countGroupAllAnswers returns the correct number of community yes in the groups', async () => {
    const answers = [
      'abc',
      '',
      'a',
      'b',
      'c',
      '',
      'ab',
      'ac',
      '',
      'a',
      'a',
      'a',
      'a',
      '',
      'b',
    ];
    expect(await countGroupAllAnswers(from(answers))).toBe(6);
  });

});