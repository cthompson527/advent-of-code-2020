import { from, Observable } from 'rxjs';
import { map, reduce, tap } from 'rxjs/operators';
import { groupInputChunks } from '../day4/passportVerification';

function removeDuplicates(groupAnswer: string) {
  return Array.from(new Set(groupAnswer)).join('');
}

function communalAnswers(answerInput: string) {
  let communalAnswer = '';
  from(answerInput.split(' ')).pipe(
    reduce((communityAnswers, v) => {
      const s = new Set(v);
      return [...communityAnswers].filter(x => s.has(x)).join('');
    })
  ).subscribe(v => communalAnswer = v);
  return communalAnswer;
}

async function countGroupAnswers(answers$: Observable<string>) {
  return answers$.pipe(
    groupInputChunks,
    map(v => v.replace(/ /g, '')),
    map(removeDuplicates),
    map(v => v.length),
    reduce(((sum, v) => sum + v), 0),
  ).toPromise();
}

async function countGroupAllAnswers(answers$: Observable<string>) {
  return answers$.pipe(
    groupInputChunks,
    map(communalAnswers),
    map(v => v.length),
    reduce(((sum, v) => sum + v), 0),
  ).toPromise();
}

export {
  removeDuplicates,
  countGroupAnswers,
  countGroupAllAnswers,
  communalAnswers,
}