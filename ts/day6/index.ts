import getFileObservable from '../readFile';
import { countGroupAnswers, countGroupAllAnswers } from './yesQuestions';

const p1$ = getFileObservable<string>('input.txt');
(async () => console.log(`Part One: ${await countGroupAnswers(p1$)}`))();

const p2$ = getFileObservable<string>('input.txt');
(async () => console.log(`Part Two: ${await countGroupAllAnswers(p2$)}`))();

