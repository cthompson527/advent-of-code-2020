import { from, Observable, zip } from "rxjs";
import { concatMap, filter, find, first, map, max, mergeMap, reduce, tap, toArray } from "rxjs/operators";


function computeRow(boardingPassRow: string) {
  const maxExp = boardingPassRow.length - 1;
  return from(boardingPassRow).pipe(
    map((ch, index) => ({ ch, index })),
    filter(({ ch }) => ch === 'B'),
    reduce((sum, { index }) => sum + Math.pow(2, maxExp - index), 0),
  );
}

function computeCol(boardingPassCol: string) {
  const maxExp = boardingPassCol.length - 1;
  return from(boardingPassCol).pipe(
    map((ch, index) => ({ ch, index })),
    filter(({ ch }) => ch === 'R'),
    reduce((sum, { index }) => sum + Math.pow(2, maxExp - index), 0),
  );
}

function seatID(boardingPass: string) {
  let rValue = 0;
  zip(computeRow(boardingPass.substring(0, 7)), computeCol(boardingPass.substring(7)))
    .pipe(
      map(([row, col]) => row * 8 + col)
    )
    .subscribe(v => rValue = v);
  return rValue;
}

async function highestSeatID(boardingPasses$: Observable<string>): Promise<number> {
  return boardingPasses$.pipe(map(seatID), reduce((max, val) => Math.max(max, val))).toPromise();
}

function skippedSeat() {
  let lastValue: number | undefined;
  let valueBeforeLast: number | undefined;
  return function(v: number) {
    if (lastValue === undefined) {
      lastValue = v;
      return false;
    }

    if (valueBeforeLast === undefined) {
      valueBeforeLast = lastValue;
      lastValue = v;
      return false;
    }

    if (v - lastValue > 1) {
      return true;
    }
    valueBeforeLast = lastValue;
    lastValue = v;
    return false;
  }
}

async function findSeatID(boardingPasses$: Observable<string>): Promise<number> {
  return boardingPasses$.pipe(
    map(seatID),
    toArray<number>(),
    map<number[], number[]>((arr: number[]) => arr.sort((a, b) => a - b)),
    concatMap(v => v),
    first(skippedSeat()),
    map(v => v - 1),
  ).toPromise();
}

export {
  computeRow,
  computeCol,
  seatID,
  highestSeatID,
  findSeatID,
}