import getFileObservable from '../readFile';
import { highestSeatID, findSeatID } from './seatCalculation';

const p1$ = getFileObservable<string>('input.txt');
(async () => console.log(`Part One: ${await highestSeatID(p1$)}`))();

const p2$ = getFileObservable<string>('input.txt');
(async () => console.log(`Part Two: ${await findSeatID(p2$)}`))();

