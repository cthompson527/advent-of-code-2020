import { from } from "rxjs";
import { computeRow, computeCol, seatID, highestSeatID, findSeatID } from "./seatCalculation";

describe('day five', () => {
  test('7 characters (BFFFBBF) retrieves the row correctly', done => {
    computeRow('BFFFBBF').subscribe({
      next: v => {
        expect(v).toBe(70);
        done();
      },
    });
  });

  test('7 characters (FFFBBBF) retrieves the row correctly', done => {
    computeRow('FFFBBBF').subscribe({
      next: v => {
        expect(v).toBe(14);
        done();
      },
    });
  });

  test('3 characters (RRR) retrieves the col correctly', done => {
    computeCol('RRR').subscribe({
      next: v => {
        expect(v).toBe(7);
        done();
      },
    });
  });

  test('3 characters (RLL) retrieves the col correctly', done => {
    computeCol('RLL').subscribe({
      next: v => {
        expect(v).toBe(4);
        done();
      },
    });
  });

  test('seatID characters (BFFFBBFRRR) retrieves the seatID correctly', () => {
    expect(seatID('BFFFBBFRRR')).toBe(567);
  });

  test('highestSeatID returns the largest seat ID in the array of boarding passes', async () => {
    const passes$ = from(['BFFFBBFRRR', 'FFFBBBFRRR', 'BBFFBBFRLL']);
    const highestID = await highestSeatID(passes$);
    expect(highestID).toBe(820);
  });

  test('findSeatID returns the your seat', async () => {
    const passes$ = from(['BFFFBBFRRR', 'BFFFBBFRRL', 'BFFFBBFRLR', 'BFFFBBFRLL', 'BFFFBBFLRL', 'BFFFBBFLLR', 'BFFFBBFLLL']);
    const highestID = await findSeatID(passes$);
    expect(highestID).toBe(563);
  });
});