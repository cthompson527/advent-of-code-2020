import { findHiddenSum, runningSum } from './solution';
import fs from 'fs';

fs.readFile('input.txt', (err, data) => {
  if (err) {
    throw err;
  }
  const input = data.toString().split('\n').map(Number);
  const missingSum = runningSum(input, 25);
  if (!missingSum) {
    throw Error('missing the sum!');
  }
  console.log(`Part One: ${missingSum}`);
  console.log(`Part Two: ${findHiddenSum(input, missingSum)}`);
});
