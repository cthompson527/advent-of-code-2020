import { checkSumExists, findHiddenSum, findRunningSum, runningSum } from "./solution";

describe('day nine', () => {
  const sampleArr = [
    35,
    20,
    15,
    25,
    47,
    40,
    62,
    55,
    65,
    95,
    102,
    117,
    150,
    182,
    127,
    219,
    299,
    277,
    309,
    576,
  ];
  test('runningSum should return the first value that is not a sum of any two of the previous n', () => {
    expect(runningSum(sampleArr, 5)).toBe(127);
  });

  test('checkSumExists will return true if two numbers in the array add up to equal the value of the sum', () => {
    expect(checkSumExists(sampleArr, 219)).toBe(true);
    expect(checkSumExists(sampleArr, 404)).toBe(true);
    expect(checkSumExists(sampleArr, 151)).toBe(false);
  });

  test('findRunningSum will return a contiguous list in the array that sums to the target sum', () => {
    expect(findRunningSum(sampleArr, 127)).toEqual([15, 25, 47, 40]);
  });

  test('findHiddenSum will return the sum of the two hidden contiguous array', () => {
    expect(findHiddenSum(sampleArr, 127)).toEqual(62);
  });
});