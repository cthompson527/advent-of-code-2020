function runningSum(array: number[], backwardsAmount: number) {
  const checkingArray = [];
  for (let i = 0; i < backwardsAmount; i++) {
    checkingArray.push(array[i]);
  }

  for (let i = backwardsAmount; i < array.length; i++) {
    if (!checkSumExists(checkingArray, array[i])) {
      return array[i];
    }
    checkingArray.shift();
    checkingArray.push(array[i]);
  }

}

function checkSumExists(array: number[], sum: number) {
  const termOne = new Set(array);
  for (let i = 0; i < array.length; i++) {
    if (termOne.has(sum - array[i])) {
      return true;
    }
  }
  return false;
}

function findRunningSum(array: number[], targetSum: number) {
  const runningArray: number[] = [];
  for (let i = 0; i < array.length; i++) {
    runningArray.push(array[i]);

    let sum = runningArray.reduce((acc, v) => acc + v, 0);
    // console.log(runningArray, sum);
    if (sum === targetSum && runningArray.length > 1) {
      return runningArray;
    }

    while (sum > targetSum) {
      runningArray.shift();
      sum = runningArray.reduce((acc, v) => acc + v, 0);
      if (sum === targetSum && runningArray.length > 1) {
        return runningArray;
      }
    }
  }
  return []
}

function findHiddenSum(array: number[], targetSum: number) {
  const runningSumArray = findRunningSum(array, targetSum);
  const min = runningSumArray.reduce((m, val) => m < val ? m : val);
  const max = runningSumArray.reduce((m, val) => m > val ? m : val);
  return min + max;
}

export {
  runningSum,
  checkSumExists,
  findRunningSum,
  findHiddenSum,
};