import getFileObservable from '../readFile';
import { findCorruptedInstruction, findInfiniteLoop } from './solution';
import fs from 'fs';

fs.readFile('input.txt', (err, data) => {
  if (err) {
    throw err;
  }
  const input = data.toString().split('\n');
  console.log(`Part One: ${findInfiniteLoop(input)}`);
  console.log(`Part Two: ${findCorruptedInstruction(input)}`);
});
