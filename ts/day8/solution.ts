function parseInstruction(instructionStr: string) {
  const instruction = instructionStr.substring(0, 3);
  const amount = Number(instructionStr.substring(4));
  return {
    instruction,
    amount,
  };
}

function findInfiniteLoop(instructions: string[]) {
  let acc = 0;
  let ptr = 0;
  const previouslyVisited: { [key: number]: boolean } = {};

  function visit(pointer: number) {
    const { instruction, amount } = parseInstruction(instructions[pointer]);
    switch (instruction) {
      case 'jmp': ptr = ptr + amount; break;
      case 'acc': ptr = ptr + 1; acc = acc + amount; break;
      case 'nop': ptr = ptr + 1; break;
      default: throw Error('invalid instruction');
    }
  }

  while (!previouslyVisited[ptr]) {
    previouslyVisited[ptr] = true;
    visit(ptr);
  }

  return acc;
}

function findCorruptedInstruction(instructions: string[]) {
  for (let swappedIndex = 0; swappedIndex < instructions.length; swappedIndex++) {
    let acc = 0;
    let ptr = 0;
    const previouslyVisited: { [key: number]: boolean } = {};
    const copiedInstructions = [...instructions];
    swap();

    function swap() {
      const { instruction } = parseInstruction(copiedInstructions[swappedIndex]);
      switch (instruction) {
        case 'jmp': copiedInstructions[swappedIndex] = copiedInstructions[swappedIndex].replace('jmp', 'nop'); break;
        case 'nop': copiedInstructions[swappedIndex] = copiedInstructions[swappedIndex].replace('nop', 'jmp'); break;
      }
    }

    function visit(pointer: number) {
      const { instruction, amount } = parseInstruction(copiedInstructions[pointer]);
      switch (instruction) {
        case 'jmp': ptr = ptr + amount; break;
        case 'acc': ptr = ptr + 1; acc = acc + amount; break;
        case 'nop': ptr = ptr + 1; break;
        default: throw Error('invalid instruction');
      }
    }

    while (!previouslyVisited[ptr]) {
      previouslyVisited[ptr] = true;
      visit(ptr);

      if (ptr === instructions.length) {
        return acc;
      }
    }
  }

  return -1;
}

export {
  parseInstruction,
  findInfiniteLoop,
  findCorruptedInstruction,
}