import { findCorruptedInstruction, findInfiniteLoop, parseInstruction } from "./solution";

describe('day eight', () => {
  test('parseInstruction correctly parses the instruction', () => {
    expect(parseInstruction('nop +0')).toEqual({ instruction: 'nop', amount: 0 });
    expect(parseInstruction('acc -99')).toEqual({ instruction: 'acc', amount: -99 });
    expect(parseInstruction('jmp -4')).toEqual({ instruction: 'jmp', amount: -4 });
  });

  const sampleInput = [
    'nop +0',
    'acc +1',
    'jmp +4',
    'acc +3',
    'jmp -3',
    'acc -99',
    'acc +1',
    'jmp -4',
    'acc +6',
  ];

  test('findInfiniteLoop keeps the state and execs the commands until infinite loop is detected', () => {
    expect(findInfiniteLoop(sampleInput)).toBe(5);
  });

  test('findCorruptedInstruction finds the incorrect instruction and returns accumulator', () => {
    expect(findCorruptedInstruction(sampleInput)).toBe(8);
  });
});
