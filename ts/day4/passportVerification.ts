import { concat, empty, Observable, from } from "rxjs";
import { bufferWhen, mergeMap, switchMap, map, concatMap, concatAll, filter, reduce, tap } from "rxjs/operators";
import { inspect } from "util";

function isValid({
  byr,
  iyr,
  eyr,
  hgt,
  hcl,
  ecl,
  pid,
}: DataFields) {
  return !(
    byr === undefined ||
    iyr === undefined ||
    eyr === undefined ||
    hgt === undefined ||
    hcl === undefined ||
    ecl === undefined ||
    pid === undefined
  );
}

function parseString(values: string) {
  return values.split(' ')
    .map(v => v.split(':'))
    .reduce((acc, pair) => ({ ...acc, [pair[0]]: pair[1] }), {});
}

function groupInputChunks(input$: Observable<string>) {
  const mapUntilBreak = mapIf();
  return concat(input$, from([''])).pipe(
    switchMap(mapUntilBreak),
  )
}

function mapIf() {
  let array: string[] = [];

  return function (v: string) {
    if (v === '') {
      if (array.length === 0) {
        return empty();
      }
      const vals = array.join(' ');
      array = [];
      return [vals];
    }
    array = [...array, v];
    return empty();
  }
}

function betweenCheck(value: number, rule: Array<number>) {
  return value >= rule[0] && value <= rule[1];
}

function regexCheck(value: string, rule: RegExp) {
  return rule.test(value);
}

function regexWithLabelCheck(value: string, rules: RegexWithLabel) {
  const groups = value.match(rules.regexWithLabel);
  if (!groups) {
    return false;
  }
  const g = [...groups];
  if (g.length === 0) {
    return false;
  }

  const [_, amount, label] = g;
  const v = betweenCheck(Number(amount), rules[label as ('cm' | 'in')].between);

  return betweenCheck(Number(amount), rules[label as ('cm' | 'in')].between);
}



function isValidWithRules(values: DataFields, rules: Rules) {
  if (!isValid(values)) {
    return false;
  }
  return (
    betweenCheck(Number(values.byr), rules.byr.between) &&
    betweenCheck(Number(values.iyr), rules.iyr.between) &&
    betweenCheck(Number(values.eyr), rules.eyr.between) &&
    regexWithLabelCheck(values.hgt as string, rules.hgt) &&
    regexCheck(values.hcl as string, rules.hcl.regex) &&
    regexCheck(values.ecl as string, rules.ecl.regex) &&
    regexCheck(values.pid as string, rules.pid.regex)
  );
}

async function countPassports(passport$: Observable<string>) {
  return passport$.pipe(
    groupInputChunks,
    map(parseString),
    filter(isValid),
    reduce((count) => count + 1, 0),
  ).toPromise();
}

async function countPassportsWithRules(passport$: Observable<string>, rules: Rules) {
  return passport$.pipe(
    groupInputChunks,
    map(parseString),
    filter(isValid),
    // tap(({ byr }) => console.log(`byr: ${byr}`)),
    filter(({ byr }) => Number(byr) >= 1920 && Number(byr) <= 2002),
    // tap(({ iyr }) => console.log(`\tiyr: ${iyr}`)),
    filter(({ iyr }) => Number(iyr) >= 2010 && Number(iyr) <= 2020),
    // tap(({ eyr }) => console.log(`\t\teyr: ${eyr}`)),
    filter(({ eyr }) => Number(eyr) >= 2020 && Number(eyr) <= 2030),
    // tap(({ hgt }) => console.log(`hgt: ${hgt}`)),
    filter(({ hgt }) => {
      const groups = (hgt as string).match(/^(\d+)(cm|in)$/);
      if (!groups) {
        return false;
      }

      const g = [...groups];
      const [_, amount, label] = g;
      if (label === 'cm') {
        return Number(amount) >= 150 && Number(amount) <= 193;
      }
      // console.log(`${hgt} ${Number(amount) >= 59 && Number(amount) <= 76}`);
      return Number(amount) >= 59 && Number(amount) <= 76;
    }),
    filter(({ hcl }) => /^#[0-9a-f]{6}$/.test(hcl as string)),
    filter(({ ecl }) => ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(ecl as string)),
    filter(({ pid }) => /^\d{9}$/.test(pid as string)),
    reduce((count) => count + 1, 0),
  ).toPromise();
}

interface DataFields {
  byr?: string;
  iyr?: string;
  eyr?: string;
  hgt?: string;
  hcl?: string;
  ecl?: string;
  pid?: string;
  cid?: string;
}

interface Rules {
  byr: BetweenMinMax;
  iyr: BetweenMinMax;
  eyr: BetweenMinMax;
  hgt: RegexWithLabel;
  hcl: Regex;
  ecl: Regex;
  pid: Regex;
}

interface RegexWithLabel {
  regexWithLabel: RegExp,
  cm: BetweenMinMax;
  in: BetweenMinMax;
}

interface BetweenMinMax {
  between: Array<number>;
}

interface Regex {
  regex: RegExp;
}

export {
  isValid,
  isValidWithRules,
  parseString,
  groupInputChunks,
  countPassports,
  countPassportsWithRules,
}