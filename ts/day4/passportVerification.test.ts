import { from } from 'rxjs';
import { countPassports, countPassportsWithRules, groupInputChunks, isValid, isValidWithRules, parseString } from './passportVerification';

describe('day four', () => {
  const passports = [
    'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd',
    'byr:1937 iyr:2017 cid:147 hgt:183cm',
    '',
    'iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884',
    'hcl:#cfa07d byr:1929',
    '',
    'hcl:#ae17e1 iyr:2013',
    'eyr:2024',
    'ecl:brn pid:760753108 byr:1931',
    'hgt:179cm',
    '',
    'hcl:#cfa07d eyr:2025 pid:166559648',
    'iyr:2011 ecl:brn hgt:59in',
  ];

  test('isValid should correctly determine if a passport is valid', () => {
    expect(isValid({
      ecl: 'gry',
      pid: '860033327',
      eyr: '2020',
      hcl: '#fffffd',
      byr: '1937',
      iyr: '2017',
      cid: '147',
      hgt: '183cm',
    })).toBe(true);
    expect(isValid({
      ecl: 'gry',
      pid: '860033327',
      eyr: '2020',
      hcl: '#fffffd',
      byr: '1937',
      iyr: '2017',
      cid: '147',
    })).toBe(false);
    expect(isValid({
      hcl: '#ae17e1',
      iyr: '2013',
      eyr: '2024',
      ecl: 'brn',
      pid: '760753108',
      byr: '1931',
      hgt: '179cm',
    })).toBe(true);
  });

  test('parseString parses the string and returns the correct object', () => {
    expect(
      parseString('ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm')
    ).toEqual({
      ecl: 'gry',
      pid: '860033327',
      eyr: '2020',
      hcl: '#fffffd',
      byr: '1937',
      iyr: '2017',
      cid: '147',
      hgt: '183cm',
    });
  });

  test('group input string chunks into one string', () => {
    const passport$ = from(passports);
    const arr: string[] = [];
    groupInputChunks(passport$)
      .subscribe({
        next: v => arr.push(v),
        error: console.log,
      });
    expect(arr).toEqual([
      'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm',
      'iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929',
      'hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm',
      'hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in',
    ]);
  });

  test('group input string chunks into one string with an extra newline', () => {
    const passport$ = from(passports.concat(['']));
    const arr: string[] = [];
    groupInputChunks(passport$)
      .subscribe({
        next: v => arr.push(v),
        error: console.log,
      });
    expect(arr).toEqual([
      'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm',
      'iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929',
      'hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm',
      'hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in',
    ]);
  });

  test('countPassports returns the correct number of valid passports', async () => {
    const passport$ = from(passports.concat(['']));
    const validPassports = await countPassports(passport$);
    expect(validPassports).toBe(2);
  });

  const rules = {
    byr: {
      between: [1920, 2002],
    },
    iyr: {
      between: [2010, 2020],
    },
    eyr: {
      between: [2020, 2030],
    },
    hgt: {
      regexWithLabel: /(\d+)(cm|in)/,
      cm: {
        between: [150, 190],
      },
      in: {
        between: [59, 76],
      },
    },
    hcl: {
      regex: /#[0-9a-f]{6}/,
    },
    ecl: {
      regex: /amb|blu|brn|gry|grn|hzl|oth/,
    },
    pid: {
      regex: /\d{9}/,
    },
  };

  test('isValidWithRules returns valid passports', () => {
    expect(isValidWithRules(parseString('eyr:1972 cid:100 hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926'), rules)).toBe(false);
    expect(isValidWithRules(parseString('iyr:2019 hcl:#602927 eyr:1967 hgt:170cm ecl:grn pid:012533040 byr:1946'), rules)).toBe(false);
    expect(isValidWithRules(parseString('pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980 hcl:#623a2f'), rules)).toBe(true);
  });

  test('countPassportsWithRules returns the correct number of valid passports', async () => {
    const newPassports = [
      'eyr:1972 cid:100',
      'hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926',
      '',
      'iyr:2019',
      'hcl:#602927 eyr:1967 hgt:170cm',
      'ecl:grn pid:012533040 byr:1946',
      '',
      'hcl:dab227 iyr:2012',
      'ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277',
      '',
      'hgt:59cm ecl:zzz',
      'eyr:2038 hcl:74454a iyr:2023',
      'pid:3556412378 byr:2007',
      '',
      'pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980',
      'hcl:#623a2f',
      '',
      'eyr:2029 ecl:blu cid:129 byr:1989',
      'iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm',
      '',
      'hcl:#888785',
      'hgt:164cm byr:2001 iyr:2015 cid:88',
      'pid:545766238 ecl:hzl',
      'eyr:2022',
      '',
      'iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719',
      '',
      'eyr:2033',
      'hgt:177cm pid:173cm',
      'ecl:utc byr:2029 hcl:#efcc98 iyr:2023',
      '',
      'pid:337605855 cid:249 byr:1952 hgt:155cm',
      'ecl:grn iyr:2017 eyr:2026 hcl:#866857',
      '',
      'cid:242 iyr:2011 pid:953198122 eyr:2029 ecl:blu hcl:#888785',
      '',
      'hgt:173cm hcl:#341e13',
      'cid:341',
      'pid:112086592',
      'iyr:2012 byr:2011 ecl:amb',
      'eyr:2030',
      '',
      'pid:790332032',
      'iyr:2019',
      'eyr:2023 byr:1969 ecl:brn',
      'hgt:163cm',
      'hcl:#623a2f',
      '',
      'byr:1920 eyr:2023 cid:146 pid:890112986 hgt:171cm hcl:#b6652a iyr:2017 ecl:hzl',
      '',
      'hcl:#c0946f byr:1967 cid:199 ecl:gry',
      'iyr:2012 pid:987409259 hgt:157cm eyr:2021',

    ];
    const passport$ = from(newPassports.concat(['']));
    const validPassports = await countPassportsWithRules(passport$, rules);
    expect(validPassports).toBe(8);
  });


});