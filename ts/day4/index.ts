import getFileObservable from '../readFile';
import { countPassports, countPassportsWithRules } from './passportVerification';

const p1$ = getFileObservable<string>('input.txt');
// (async () => console.log(`Part One: ${await countPassports(p1$)}`))();

const p2$ = getFileObservable<string>('input.txt');
const rules = {
  byr: {
    between: [1920, 2002],
  },
  iyr: {
    between: [2010, 2020],
  },
  eyr: {
    between: [2020, 2030],
  },
  hgt: {
    regexWithLabel: /(\d+)(cm|in)/,
    cm: {
      between: [150, 190],
    },
    in: {
      between: [59, 76],
    },
  },
  hcl: {
    regex: /#[0-9a-f]{6}/,
  },
  ecl: {
    regex: /amb|blu|brn|gry|grn|hzl|oth/,
  },
  pid: {
    regex: /\d{9}/,
  },
};
(async () => console.log(`Part Two: ${await countPassportsWithRules(p2$, rules)}`))();

