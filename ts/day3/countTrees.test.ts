import { from } from 'rxjs';
import { countTrees, isTree, countTreesMultipleSlopes, reduceCounts, multiplyResult } from './countTrees';

describe('day three', () => {
  const map = [
    '..##.',
    '#...#',
    '.#...',
    '..#.#',
    '.#...',
    '..#.#',
  ];

  test('isTree should correctly calculate a tree based on row in the map', () => {
    expect(isTree('..#.', 2)).toBe(true);
    expect(isTree('..#.', 3)).toBe(false);
    expect(isTree('..#.', 6)).toBe(true);
  });

  test('countTrees returns the correct number of trees encountered', async () => {
    const map$ = from(map);
    expect(await countTrees(map$)).toBe(2);
  });

  test('countTreesMultipleSlopes calculates the product of the number of trees encountered', async () => {
    const slopes = [
      { right: 1, down: 1 },
      { right: 3, down: 1 },
    ];
    const map$ = from(map);
    expect(await countTreesMultipleSlopes(map$, slopes)).toBe(0);
  });

  test('countTreesMultipleSlopes calculates the product of the number of trees encountered when multiple down are skipped', async () => {
    const slopes = [
      { right: 1, down: 2 },
      { right: 3, down: 1 },
    ];
    const map$ = from(map);
    expect(await countTreesMultipleSlopes(map$, slopes)).toBe(2);
  });

  test('reduceCounts correct adds to the object', () => {
    const acc = {
      '12': 2,
      '35': 5,
    }
    expect(reduceCounts(acc, { right: 1, down: 2 })).toEqual({ '12': 3, '35': 5 });
    expect(reduceCounts(acc, { right: 3, down: 5 })).toEqual({ '12': 2, '35': 6 });
    expect(reduceCounts(acc, { right: 5, down: 8 })).toEqual({ '12': 2, '35': 5, '58': 1 });
  });

  test('multiplyResults multiples all the values in the object', () => {
    expect(multiplyResult({ 1: 2, 3: 5 })).toBe(10);
    expect(multiplyResult({ 1: 2, 3: 5, 5: 9 })).toBe(90);
  });
});
