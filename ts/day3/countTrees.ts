import { from, Observable } from "rxjs";
import { map, filter, reduce, mergeMap, tap } from "rxjs/operators";

function isTree(mapRow: string, xLoc: number): boolean {
  const len = mapRow.length;
  return mapRow[xLoc % len] === '#';
}

function reduceCounts(acc: Accumulator, { right, down }: { right: number, down: number }) {
  if (!acc[`${right}${down}`]) {
    acc[`${right}${down}`] = 0;
  }
  return {
    ...acc,
    [`${right}${down}`]: acc[`${right}${down}`] + 1,
  };
}

function multiplyResult(acc: Accumulator) {
  return Object.values(acc).reduce((a, v) => a * v);
}

async function countTrees(map$: Observable<string>): Promise<number> {
  return map$.pipe(
    map((mapRow, index) => ({ mapRow, xLoc: index * 3 })),
    filter(({ mapRow, xLoc }) => isTree(mapRow, xLoc)),
    reduce((acc) => acc + 1, 0),
  ).toPromise();
}

async function countTreesMultipleSlopes(map$: Observable<string>, slopes: Slope[]): Promise<number> {
  const zeroedAcc = slopes.reduce((acc, { right, down }) => ({ ...acc, [`${right}${down}`]: 0 }), {});
  return map$.pipe(
    mergeMap((mapRow, index) =>
      from(slopes).pipe(
        filter(({ down }) => index % down === 0),
        map(({ right, down }) => ({ mapRow, xLoc: (index / down) * right, right, down })),
      ),
    ),
    filter(({ mapRow, xLoc }) => isTree(mapRow, xLoc)),
    reduce(reduceCounts, zeroedAcc),
    map(multiplyResult),
  ).toPromise();
}

export {
  isTree,
  reduceCounts,
  countTrees,
  countTreesMultipleSlopes,
  multiplyResult,
};

interface Slope {
  right: number;
  down: number;
}

interface Accumulator {
  [key: string]: number;
}