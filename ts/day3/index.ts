import getFileObservable from '../readFile';
import { countTrees, countTreesMultipleSlopes } from './countTrees';

const p1$ = getFileObservable<string>('input.txt');
(async () => console.log(`Part One: ${await countTrees(p1$)}`))();

const p2$ = getFileObservable<string>('input.txt');
const slopes = [
  { right: 1, down: 1 },
  { right: 3, down: 1 },
  { right: 5, down: 1 },
  { right: 7, down: 1 },
  { right: 1, down: 2 },
];
(async () => console.log(`Part Two: ${await countTreesMultipleSlopes(p2$, slopes)}`))();

