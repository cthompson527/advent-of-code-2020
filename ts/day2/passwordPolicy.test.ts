import { from } from 'rxjs';
import { isPasswordValidPolicy, numTimesInPassword, passwordPolicyOne, passwordPolicyTwo, splitSections } from './passwordPolicy';

describe('day two', () => {
  const sample = ['1-3 a: abcde', '1-3 b: cdefg', '2-9 c: ccccccccc'];
  test('split sections will properly split the line into the correct values', () => {
    expect(splitSections(sample[0])).toEqual(['1', '3', 'a', 'abcde']);
    expect(splitSections(sample[1])).toEqual(['1', '3', 'b', 'cdefg']);
    expect(splitSections(sample[2])).toEqual(['2', '9', 'c', 'ccccccccc']);
  });

  test('isPasswordValidPolicy function will correctly assess the password', () => {
    const sampleArrs = [
      ['1', '3', 'a', 'abcde'],
      ['1', '3', 'b', 'cdefg'],
      ['2', '9', 'c', 'ccccccccc']
    ];
    expect(isPasswordValidPolicy(sampleArrs[0])).toEqual(true);
    expect(isPasswordValidPolicy(sampleArrs[1])).toEqual(false);
    expect(isPasswordValidPolicy(sampleArrs[2])).toEqual(true);
  });

  test('numTimesInPassword function will count the number of times a character is in a string', () => {
    const sampleArrs: Array<[string, string, number]> = [
      ['a', 'abcde', 1],
      ['b', 'cdefg', 0],
      ['c', 'ccccccccc', 9],
    ];
    sampleArrs.forEach(([ch, password, expected]) => expect(numTimesInPassword(ch, password)).toEqual(expected));
  });


  test('the example for part one must be correct', async () => {
    const password$ = from(sample);
    expect(await passwordPolicyOne(password$)).toEqual(2);
  });

  test('the example for part two must be correct', async () => {
    const password$ = from(sample);
    expect(await passwordPolicyTwo(password$)).toEqual(1);
  });
});