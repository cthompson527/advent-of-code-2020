import { Observable } from "rxjs";
import { filter, map, reduce } from "rxjs/operators";

async function passwordPolicyOne(password$: Observable<string>): Promise<number> {
  return await password$.pipe(
    map(splitSections),
    filter(isPasswordValidPolicy),
    reduce((acc, _) => acc + 1, 0)
  ).toPromise();
}

async function passwordPolicyTwo(password$: Observable<string>): Promise<number> {
  return await password$.pipe(
    map(splitSections),
    filter(([iOne, iTwo, ch, password]) =>
      password[Number(iOne) - 1] !== password[Number(iTwo) - 1] &&
      (password[Number(iOne) - 1] === ch || password[Number(iTwo) - 1] === ch)
    ),
    reduce((acc, _) => acc + 1, 0)
  ).toPromise();
}


const regex = /^(\d+)-(\d+) ([a-z]): ([a-z]+)$/g;

function splitSections(line: string): string[] {
  const groups = Array.from(line.matchAll(regex), m => m.slice(1, 5))[0];
  return groups;
}

function isPasswordValidPolicy([min, max, ch, password]: string[]) {
  const numTimes = numTimesInPassword(ch, password);
  const [m, mm] = [Number(min), Number(max)];
  return m <= numTimes && numTimes <= mm;
}

function isPasswordValidNewPolicy([min, max, ch, password]: string[]) {

}

function numTimesInPassword(ch: string, password: string): number {
  return Array.from(password).filter(v => v === ch).length;
}

export {
  passwordPolicyOne,
  passwordPolicyTwo,
  splitSections,
  isPasswordValidPolicy,
  numTimesInPassword,
}