import getFileObservable from '../readFile';
import { passwordPolicyOne, passwordPolicyTwo } from './passwordPolicy';

const p1$ = getFileObservable<string>('input.txt');
(async () => console.log(`Part One: ${await passwordPolicyOne(p1$)}`))();

const p2$ = getFileObservable<string>('input.txt');
(async () => console.log(`Part Two: ${await passwordPolicyTwo(p2$)}`))();
