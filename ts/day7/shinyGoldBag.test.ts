import { from } from "rxjs";
import { bagNumber, countBagsIncludingParent, emitBagsStart, numberOfBags, parseInput, parseInputString, parseInputStringReverseRelationship, splitCounts } from "./shinyGoldBag";

describe('day seven', () => {
  const sampleInput = [
    'light red bags contain 1 bright white bag, 2 muted yellow bags.',
    'dark orange bags contain 3 bright white bags, 4 muted yellow bags.',
    'bright white bags contain 1 shiny gold bag.',
    'muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.',
    'shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.',
    'dark olive bags contain 3 faded blue bags, 4 dotted black bags.',
    'vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.',
    'faded blue bags contain no other bags.',
    'dotted black bags contain no other bags.',
  ];

  test('parseInputString can parse a sentence into json', () => {
    expect(parseInputString(sampleInput[0])).toEqual({
      'bright white': ['light red'],
      'muted yellow': ['light red'],
      'light red': [],
    });
  });

  test('parseInputString can parse a sentence with "no other bags"', () => {
    expect(parseInputString(sampleInput[7])).toEqual({
      'faded blue': [],
    });
  });

  test('parseInput parses the entire input array', () => {
    expect(parseInput(sampleInput, parseInputString)).toEqual({
      'faded blue': [
        'muted yellow',
        'dark olive',
        'vibrant plum',
      ],
      'dotted black': [
        'dark olive',
        'vibrant plum',
      ],
      'vibrant plum': [
        'shiny gold',
      ],
      'dark olive': [
        'shiny gold',
      ],
      'shiny gold': [
        'bright white',
        'muted yellow',
      ],
      'muted yellow': [
        'light red',
        'dark orange',
      ],
      'bright white': [
        'light red',
        'dark orange',
      ],
      'dark orange': [],
      'light red': [],
    });
  });

  test('emitBags emits all the bags that can contain a shiny bag', () => {
    const rules = parseInput(sampleInput, parseInputString);
    expect(emitBagsStart(rules, 'shiny gold')).toEqual([
      'bright white',
      'muted yellow',
      'light red',
      'dark orange',
    ]);
  });

  test('bagNumber shows the correct number of bags that can hold a shiny bag', async () => {
    expect(await bagNumber(from(sampleInput))).toBe(4);
  });

  test('parseInputStringReverseRelationship parses a line with reverse relationship correctly', () => {
    expect(parseInputStringReverseRelationship(sampleInput[0])).toEqual({
      'light red': ['1 bright white', '2 muted yellow']
    });
  });

  test('parseInput will parses the array correctly when using parseInputReverseString', () => {
    expect(parseInput(sampleInput, parseInputStringReverseRelationship)).toEqual({
      'light red': ['1 bright white', '2 muted yellow'],
      'dark orange': ['3 bright white', '4 muted yellow'],
      'bright white': ['1 shiny gold'],
      'muted yellow': ['2 shiny gold', '9 faded blue'],
      'shiny gold': ['1 dark olive', '2 vibrant plum'],
      'dark olive': ['3 faded blue', '4 dotted black'],
      'vibrant plum': ['5 faded blue', '6 dotted black'],
      'faded blue': [],
      'dotted black': [],
    });
  });

  test('splitCounts splits the count and the bag type', () => {
    expect(splitCounts('1 bright white')).toEqual({ amount: 1, type: 'bright white' });
    expect(splitCounts('12 vibrant plum')).toEqual({ amount: 12, type: 'vibrant plum' });
  });

  test('countBags returns the number of bags inside of a starting bag', () => {
    const rules = parseInput(sampleInput, parseInputStringReverseRelationship);
    expect(countBagsIncludingParent(rules, 'shiny gold') - 1).toBe(32);
  });

  test('numberOfBags returns the correct number of bags from observable', async () => {
    expect(await numberOfBags(from(sampleInput))).toBe(32);
  });
});