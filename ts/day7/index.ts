import getFileObservable from '../readFile';
import { bagNumber, numberOfBags } from './shinyGoldBag';

const p1$ = getFileObservable<string>('input.txt');
(async () => console.log(`Part One: ${await bagNumber(p1$)}`))();

const p2$ = getFileObservable<string>('input.txt');
(async () => console.log(`Part Two: ${await numberOfBags(p2$)}`))();

