import { count } from "console";
import { from, Observable } from "rxjs";
import { concatMap, map, mergeMap, reduce, scan } from "rxjs/operators";

function parseInputString(inputString: string): { [key: string]: string[] } {
  inputString = inputString.substring(0, inputString.length - 1);
  let [outside, inside] = inputString.split('contain');
  outside = outside.replace('bags', '').trim();
  if (inside.trim() === 'no other bags') {
    return { [outside]: [] };
  }
  const insideArray = inside.split(',').map(i => i
    .replace(/bags?/, '')
    .trim()
    .replace(/^\d+ /, '')
    .trim()
  );
  return insideArray.reduce((builtObj, value) => ({ ...builtObj, [value]: [outside], [outside]: [] }), {});
}

type ParseFn = (iStr: string) => { [key: string]: string[] };

function parseInput(input: string[], parseFn: ParseFn) {
  let returnValue = {};
  from(input).pipe(
    map(parseFn),
    reduce((acc: { [key: string]: string[] }, val) => {
      const joinedVals = Object.keys(val)
        .reduce((prev, key) => {
          const retVal: { [key: string]: string[] } = {};
          if (acc[key]) {
            retVal[key] = acc[key].concat(val[key]);
          } else {
            retVal[key] = val[key];
          }
          return { ...prev, ...retVal };
        }, {});
      return { ...acc, ...joinedVals };
    }, {})
  ).subscribe({
    next: v => returnValue = v,
  });
  return returnValue;
}

function emitBags(rules: { [key: string]: string[] }, value: string): string[] {
  return rules[value].flatMap(v => {
    return rules[v] ? rules[value].concat(emitBags(rules, v)) : []
  });
}

function emitBagsStart(rules: { [key: string]: string[] }, initial: string) {
  const allBags = emitBags(rules, initial);
  return Array.from(new Set(allBags));
}

async function bagNumber(fileInput$: Observable<string>) {
  const inputArr = await fileInput$.pipe(
    reduce((acc: string[], inputVal) => ([...acc, inputVal]), []),
  ).toPromise();

  const rules = parseInput(inputArr, parseInputString);
  return emitBagsStart(rules, 'shiny gold').length;
}

function parseInputStringReverseRelationship(inputString: string): { [key: string]: string[] } {
  inputString = inputString.substring(0, inputString.length - 1);
  let [outside, inside] = inputString.split('contain');
  outside = outside.replace('bags', '').trim();
  if (inside.trim() === 'no other bags') {
    return { [outside]: [] };
  }
  const insideArray = inside.split(',').map(i => i
    .replace(/bags?/, '')
    .trim()
  );
  return { [outside]: insideArray };
}

function splitCounts(inputString: string) {
  const result = /^(\d+) (\D+)$/g.exec(inputString);
  if (!result) {
    throw Error(`could not parse ${inputString}`);
  }
  const amount = Number(result[1]);
  const type = result[2];

  return { amount, type };
}

function countBagsIncludingParent(rules: { [key: string]: string[] }, parentType: string) {
  let total = 1;
  for (const current of rules[parentType]) {
    const { amount, type } = splitCounts(current);
    total = total + amount * countBagsIncludingParent(rules, type);
  }
  return total;
}

async function numberOfBags(fileInput$: Observable<string>) {
  const inputArr = await fileInput$.pipe(
    reduce((acc: string[], inputVal) => ([...acc, inputVal]), []),
  ).toPromise();

  const rules = parseInput(inputArr, parseInputStringReverseRelationship);
  return countBagsIncludingParent(rules, 'shiny gold') - 1;
}

export {
  parseInputString,
  parseInput,
  emitBagsStart,
  bagNumber,
  parseInputStringReverseRelationship,
  splitCounts,
  countBagsIncludingParent,
  numberOfBags,
};