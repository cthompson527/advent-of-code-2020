import { Observable } from "rxjs";
import { filter, map, mergeMap } from 'rxjs/operators';

async function calculateExpensesOne(expense1$: Observable<number>, expense2$: Observable<number>): Promise<number> {
  const { v1, v2 } = await expense1$.pipe(
    mergeMap(v1 => expense2$.pipe(map(v2 => ({ v1, v2 })))),
    map(({ v1, v2 }) => ({ v1: Number(v1), v2: Number(v2) })),
    filter(({ v1, v2 }) => v1 + v2 == 2020)
  ).toPromise();
  return v1 * v2;
}

async function calculateExpensesTwo(expense1$: Observable<number>, expense2$: Observable<number>, expense3$: Observable<number>): Promise<number> {
  const { v1, v2, v3 } = await expense1$.pipe(
    mergeMap(v1 => expense2$.pipe(map(v2 => ({ v1, v2 })))),
    map(({ v1, v2 }) => ({ v1: Number(v1), v2: Number(v2) })),
    mergeMap(({ v1, v2 }) => expense3$.pipe(map(v3 => ({ v1, v2, v3: Number(v3) })))),
    filter(({ v1, v2, v3 }) => v1 + v2 + v3 == 2020)
  ).toPromise();
  return v1 * v2 * v3;
}


export {
  calculateExpensesOne,
  calculateExpensesTwo,
}
