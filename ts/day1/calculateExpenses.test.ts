import { from } from 'rxjs';
import { calculateExpensesOne, calculateExpensesTwo } from './calculateExpenses';

describe('day one', () => {
  const sample = [1721, 979, 366, 299, 675, 1456];
  test('the example for part one must be correct', async () => {
    const expense1$ = from(sample);
    const expense2$ = from(sample);
    expect(await calculateExpensesOne(expense1$, expense2$)).toBe(514579);
  });

  test('the example for part two must be correct', async () => {
    const expense1$ = from(sample);
    const expense2$ = from(sample);
    const expense3$ = from(sample);
    expect(await calculateExpensesTwo(expense1$, expense2$, expense3$)).toBe(241861950);
  });
});