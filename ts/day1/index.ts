import getFileObservable from '../readFile';
import { calculateExpensesOne, calculateExpensesTwo } from './calculateExpenses';
require('events').defaultMaxListeners = 0;

const p1a$ = getFileObservable<number>('input.txt');
const p1b$ = getFileObservable<number>('input.txt');
(async () => console.log(`Part One: ${await calculateExpensesOne(p1a$, p1b$)}`))();

const p2a$ = getFileObservable<number>('input.txt');
const p2b$ = getFileObservable<number>('input.txt');
const p2c$ = getFileObservable<number>('input.txt');
(async () => console.log(`Part Two: ${await calculateExpensesTwo(p2a$, p2b$, p2c$)}`))();
